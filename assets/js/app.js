/**
 * 
 * JQuery File To Create Dynamic Graphs/Charts
 * Using : JQuery and Highchart
 * Auther: Raj-Uimatic
 * Date: 11th-Feb-2016
 * 
 */


var drawStudentChart = function ($config) {
    $this = this;

    // creating delemeter variables
    this.delimiter = {
        coordinate: ',',
        point: ';',
        line: '|'
    };

    this.selector = $($config.selector);

    this.xmlData = [];

    this.chart = '';

// this function must be fired on crating object 
// for now I am passing a url to fetch data of xml On live environment we can supply directly xml in the function which will remove our ajax call
    this.init = function (url) {
        $.post(url, {}, function (xml) {
            xmlDoc = $.parseXML(xml);
            $this.xmlData = $this.getChartParam(xmlDoc);
            $this.xmlData['type'] = $this.getChartType(xml);
            $this.xmlData['delimiter'] = $this.delimiter;
            $this.chart = $this.drawChart($this.xmlData);
        }, 'html');
    };

    // This function fetches all the params from xml data
    this.getChartParam = function (xml) {
        var xmlParam = $(xml).find('param');
        // need to convert this array ro object
        var xmlData = [];
        xmlParam.each(function (k, v) {
            xmlData[$(v).attr('name')] = $(v).attr('value');
        });
        return xmlData;
    };

// This function gets graph (line/dotted) base one the <selectPointInteraction> class name in xml data
    this.getChartType = function (xml) {
        var xmlParam = $(xml).find('selectPointInteraction');
        var interactionClass = $(xmlParam).attr('class');
        return (interactionClass == 'placePointsSelectPointInteraction') ? 'point' : 'line';
    };

// This function crates hichart as you can see I am using function uim_chart here. You may find this function/plugin in file assets/js/uim_chart.js 
    this.drawChart = function (xmlData) {
        return this.selector.uim_chart(xmlData);
    };

// This function fatches chart ploted points
    this.getSeriesData = function () {
        var chart = this.selector.highcharts();
        var data = '';
        if (this.xmlData['type'] == 'line') {
            this.nullData = true;
            $.each(chart.series[0].data, function (key, value) {
                var x = value.x;
                var y = value.y;
                if (x == null || y == null) {
                    data += '|';
                    $this.nullData = false;
                } else {
                    var point = ($this.nullData) ? $this.delimiter.point : '';
                    data += (data == '') ? x + $this.delimiter.coordinate + y : point + x + $this.delimiter.coordinate + y;
                    $this.nullData = true;
                }
            });
            data = data.replace('||','|');
        } else {
            $.each(chart.series[0].data, function (key, value) {
                var x = value.x;
                var y = value.y;
                data += (data == '') ? x + $this.delimiter.coordinate + y : $this.delimiter.point + x + $this.delimiter.coordinate + y;
            });
        }

        return data;
    };

    this.setSeriesData = function (data) {
        var chart = this.selector.highcharts();
        var newData = this.filterSeriesData(data);
        chart.series[0].setData(newData);
    };

    // This function filter series data and convert data in a format which high-chat accepts
    this.filterSeriesData = function (dataString) {
        var chartData = [];
        if (this.xmlData['type'] == 'line') {
            var dataArray = dataString.split(this.delimiter.line);
            for (var i = 0; i < dataArray.length; i++) {
                var lineCoordinates = dataArray[i];
                lineCoordinatesXY = lineCoordinates.split(this.delimiter.point);
                $.each(lineCoordinatesXY, function (k, v) {
                    xAndY = v.split($this.delimiter.coordinate);
                    chartData.push([parseFloat(xAndY[0]), parseFloat(xAndY[1])]);
                })
                chartData.push([null, null]);
            }
        } else {
            var dataArray = dataString.split(this.delimiter.point);
            for (var i = 0; i < dataArray.length; i++) {
                xAndY = dataArray[i].split(this.delimiter.coordinate);
                chartData.push([parseFloat(xAndY[0]), parseFloat(xAndY[1])]);
            }
        }
        return chartData;
    };

    this.valdatePoint = function (x, y) {
        var error = [];

        if (x > $this.xmlData.xAxisMaxValue) {
            error.push('Value of x-coordinate ' + i + 'th point( ' + x + ' ) must be less than ' + $this.xmlData.xAxisMaxValue)
        }

        if (x < $this.xmlData.xAxisMinValue) {
            error.push('Value of x-coordinate ' + i + 'th point( ' + x + ' ) must be less than ' + $this.xmlData.xAxisMaxValue)
        }


        if (x > $this.xmlData.yAxisMaxValue) {

        }
    };

};
