/**
 * 
 * Creating a uim_chart plugin which will be responsible for creating/updating chart according to the supplyed data i.e. $xmlConfig
 * Using : JQuery and Highchart
 * Auther: Raj-Uimatic
 * Date: 11th-Feb-2016
 * 
 */

(function ($) {

    $.fn.uim_chart = function ($xmlConfig) {
        // Default configuration of chart
        this.config = {
            gridVisible: 1,
            gridWidthInPixels: 600,
            gridHeightInPixels: 600,
            xAxisLabelPattern: 1,
            xAxisMaxValue: 10,
            xAxisMinValue: 0,
            xAxisStepValue: 1,
            xAxisTitle: "X",
            yAxisLabelPattern: 1,
            yAxisMaxValue: 10,
            yAxisMinValue: 0,
            yAxisStepValue: 1,
            yAxisTitle: "Y",
            type: 'line'
        };
        // updating default config according to the config data supplyed from xml file 
        $.extend(this.config, $xmlConfig);


        this.config.delimiter = $xmlConfig['delimiter'];

        var $this = this;
        var elem = $(this);
        return new studentChart(elem, $this.config)

    };


    // main chart class

    function studentChart(elem, $config) {
        var $this = this;
        var pointTolerance = 0.25; // how close to nearest point for us to "snap to grid"

        var pointToleranceSnapToGrid = function (pointVal) {
            var numToRoundTo;
            if (typeof pointTolerance !== 'undefined' && typeof pointTolerance !== 'null' && $.isNumeric(pointTolerance)) {
                numToRoundTo = 1 / (pointTolerance);
                return Math.round(pointVal * numToRoundTo) / numToRoundTo;
            } else {
                return pointVal;
            }
        };

        this.chartSelector = elem.highcharts({
            chart: {
                type: "scatter",
                width: $config.gridWidthInPixels,
                height: $config.gridHeightInPixels,
                showAxes: true,
                events: {
                    click: function (e) {
                        if ($config.type != 'line') {
                            if (this.series[0].data.length == 2) {
                                this.series[0].data[0].remove();
                            } else if (this.series[0].data.length > 2) {
                                this.series[0].setData([[0, 0]]);
                            }
                        }
                        // find the clicked values and the series
                        var x = e.xAxis[0].value,
                                y = e.yAxis[0].value,
                                series = this.series[0];
                        //this.series[0].color = 'red';
                        this.series[0].lineWidth = 15;
                        // Add it
//                        console.log("rounding ", x, y, pointToleranceSnapToGrid(x), pointToleranceSnapToGrid(y));
                        if ($config.type == 'line' && this.series[0].data.length > 1) {
                            series.addPoint([null, null]);
                            series.addPoint([pointToleranceSnapToGrid(x), pointToleranceSnapToGrid(y)]);
                            series.addPoint([pointToleranceSnapToGrid(x) + 1, pointToleranceSnapToGrid(y) + 1]);
                        } else {
                            series.addPoint([pointToleranceSnapToGrid(x), pointToleranceSnapToGrid(y)]);
                        }

                        $this.chart.xAxis[0].setExtremes(parseFloat($config.xAxisMinValue), parseFloat($config.xAxisMaxValue));
                        $this.chart.yAxis[0].setExtremes(parseFloat($config.yAxisMinValue), parseFloat($config.yAxisMaxValue));
                    }
                }
            },
            title: {
                text: 'UIM chart prototype'
            },
            tooltip: {
                enabled: true,
                backgroundColor: 'none',
                borderWidth: 1
            },
            xAxis: {
                gridLineWidth: 1,
                max: $config.xAxisMaxValue,
                minRange: $config.xAxisMinValue,
                tickInterval: $config.xAxisStepValue,
                title: {text: $config.xAxisTitle}
            },
            yAxis: {
                max: $config.yAxisMaxValue,
                min: $config.yAxisMinValue,
                tickInterval: $config.yAxisStepValue,
                title: {text: $config.yAxisTitle}
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    lineWidth: ($config.type == 'line') ? 1 : 0,
                    animation: false,
                    point: {
                        events: {
                            'click': function () {
                                if (this.series.data.length > 1) {
                                    this.remove();
                                }
                            }
                        }
                    }
                },
                line: {
                    animation: {
                        duration: 0
                    }
                }
            },
            series: [{
                    name: 'Student',
                    animation: false,
                    color: 'rgba(223, 83, 83, .5)',
                    data: [[0, 0]]
                }]
        });

        // create a highchart object
        this.chart = this.chartSelector.highcharts();

    }
    ;


}(jQuery));